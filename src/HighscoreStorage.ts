import { Difficulty } from "./EnemySpawner.js";

export class Highscore {
    public playerName: string;
    public difficulty: Difficulty;
    public score: number;
    public date: number;

    constructor(playerName: string, difficulty: Difficulty, score: number) {
        this.playerName = playerName;
        this.difficulty = difficulty;
        this.score = score;
        this.date = Date.now();
    }
}

class HighscoreStorage {
    private storage: Storage;
    private highscores: Highscore[] = [];

    private get LatestHighscore() { return this.highscores[this.highscores.length - 1]; }

    public get LastUsedDifficulty() {
        let latestHighscore: Highscore | undefined = this.LatestHighscore;
        return latestHighscore ? latestHighscore.difficulty : Difficulty.Medium;
        //return this.LatestHighscore?.difficulty ?? Difficulty.Easy; in TypeScript 3.7
    }

    public get LastUsedPlayerName() {
        let latestHighscore: Highscore | undefined = this.LatestHighscore;
        return latestHighscore ? latestHighscore.playerName : "Player1";
        //return this.LatestHighscore?.playerName ?? "Player1"; in TypeScript 3.7
    }

    constructor() {
        this.storage = window.localStorage;
        for (let i: number = 0; i < this.storage.length; i++)
            this.highscores.push(JSON.parse(<string>this.storage.getItem(i.toString())));
    }

    public getOrderedHighscores(difficulty: Difficulty): Highscore[] {
        return this.highscores.filter((highscore: Highscore) => highscore.difficulty == difficulty).sort((a: Highscore, b: Highscore) => a.date.valueOf() - b.date.valueOf()).sort((a: Highscore, b: Highscore) => b.score - a.score);
    }

    public addHighscore(highscore: Highscore): void {
        this.highscores.push(highscore);
        this.storage.setItem(this.storage.length.toString(), JSON.stringify(highscore));
    }
}

export const highscoreStorage: HighscoreStorage = new HighscoreStorage();
