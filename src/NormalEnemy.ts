import { Point, Vector, contains } from "./Geometry.js";
import { DrawingContext, IDrawable } from "./Drawing.js";

export class NormalEnemy extends Vector implements IDrawable {
    protected orientation: Vector;
    protected entered: boolean;
    protected small: boolean;

    public get Dir(): Vector { return this.orientation; }

    public constructor(spawn: Point, target: Point, small: boolean) {
        let vecLength: number = 4;
        let dirLength: number = 42;

        if (small) {
            vecLength = 8;
            dirLength = 21;
        }

        let v: Vector = new Vector(spawn.x, spawn.y, 1, 0);
        v.setOrientation(target);

        super(spawn.x, spawn.y, v.Xvec * vecLength, v.Yvec * vecLength);

        this.orientation = new Vector(spawn.x, spawn.y, v.Xvec * dirLength, v.Yvec * dirLength);

        this.entered = false;
        this.small = small;
    }

    public move(context: DrawingContext): void {
        let corners: Point[] = this.Dir.getRotationPoints([90, 90, 90, 90]);
        let horizontalAction: boolean = true, verticalAction: boolean = true;

        corners.forEach((corner: Point): void => {
            if (this.entered && horizontalAction && !context.isHorizontallyInside(corner, this.xvec > 0 ? 1 : -1)) { // inline if prevents wall stucking
                this.xvec *= -1;
                horizontalAction = false;
            }

            if (this.entered && verticalAction && !context.isVerticallyInside(corner, this.yvec > 0 ? 1 : -1)) { // inline if prevents wall stucking
                this.yvec *= -1;
                verticalAction = false;
            }
        });

        if (!this.entered && context.isSquareInside(this.Dir))
            this.entered = true;

        this.x += this.xvec;
        this.y += this.yvec;
        this.orientation.x = this.x;
        this.orientation.y = this.y;
    }

    public contains(point: Point): boolean {
        return contains(this.orientation.getRotationPoints([90, 90, 90, 90]), point);
    }

    public touches(vector: Vector): boolean {
        return this.contains(vector) || this.contains(vector.Endpoint);
    }

    public hit(): NormalEnemy[] {
        let earr: NormalEnemy[] = [];

        if (this.entered && !this.small) {
            for (let i: number = 0; i < 4; i++) {
                earr[i] = new NormalEnemy(new Point(this.orientation.x + this.orientation.Xvec / 2, this.orientation.y + this.orientation.Yvec / 2), this.orientation.Endpoint, true);
                this.orientation.rotate(90);
            }
        }
        
        return earr;
    }

    public draw(context: DrawingContext): void {
        context.drawPolygon(this.orientation.getRotationPoints([90, 90, 90, 90]));
    }
}