import { Point } from "./Geometry.js";
import { DrawingContext, IDrawable } from "./Drawing.js";

export class Cursor extends Point implements IDrawable {
    public updatePosition(event: MouseEvent): void {
        let canvas: HTMLCanvasElement = <HTMLCanvasElement>event.srcElement;
        let rect: ClientRect = canvas.getBoundingClientRect();
        this.x = event.clientX - rect.left;
        this.y = event.clientY - rect.top;
    }

    public draw(context: DrawingContext): void {
        context.drawLine(new Point(this.x - 10, this.y), new Point(this.x + 10, this.y));
        context.drawLine(new Point(this.x, this.y - 10), new Point(this.x, this.y + 10));
    }
};