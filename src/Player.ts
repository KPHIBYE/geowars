import { Vector, Point } from "./Geometry.js";
import { DrawingContext, IDrawable } from "./Drawing.js";
import { Shot } from "./Shot.js";

export enum ShotControl {
    Mouse,
    ArrowKeys
}

enum Direction {
    Up,
    Left,
    Down,
    Right
}

export class Player extends Vector implements IDrawable {
    protected w: boolean = false; protected a: boolean = false; protected s: boolean = false; protected d: boolean = false;
    protected lastArrowKey: Direction = Direction.Up;
    public shotControl: ShotControl = ShotControl.Mouse;

    public constructor() {
        super(0, 0, 0, -17);

        window.addEventListener("keydown", this.keyDownHandler.bind(this));
        window.addEventListener("keyup", this.keyUpHandler.bind(this));
    }

    private updateDirection(): void {
        if (this.shotControl == ShotControl.Mouse) return;

        let targetDirection: Point = this.clone();

        switch (this.lastArrowKey) {
            case Direction.Up:
                targetDirection.y -= 17;
                break;
            case Direction.Left:
                targetDirection.x -= 17;
                break;
            case Direction.Down:
                targetDirection.y += 17;
                break;
            case Direction.Right:
                targetDirection.x += 17;
                break;
            default:
                targetDirection.y -= 17;
                break;
        }

        this.setOrientation(targetDirection);
    }

    public keyDownHandler(event: KeyboardEvent): void {
        switch (event.keyCode) {
            case 87: // w
                this.w = true;
                break;
            case 38: // ArrowUp
                this.lastArrowKey = Direction.Up;
                this.updateDirection();
                break;
            case 65: // a
                this.a = true;
                break;
            case 37: // ArrowLeft
                this.lastArrowKey = Direction.Left;
                this.updateDirection();
                break;
            case 83: // s
                this.s = true;
                break;
            case 40: // ArrowDown
                this.lastArrowKey = Direction.Down;
                this.updateDirection();
                break;
            case 68: // d
                this.d = true;
                break;
            case 39: // ArrowRight
                this.lastArrowKey = Direction.Right;
                this.updateDirection();
                break;
        }
    }

    private keyUpHandler(event: KeyboardEvent): void {
        switch (event.keyCode) {
            case 87: // w
                this.w = false;
                break;
            //case 38: // ArrowUp
            //    //this.au = false;
            //    break;
            case 65: // a
                this.a = false;
                break;
            //case 37: // ArrowLeft
            //    //this.al = false;
            //    break;
            case 83: // s
                this.s = false;
                break;
            //case 40: // ArrowDown
            //    //this.ad = false;
            //    break;
            case 68: // d
                this.d = false;
                break;
            //case 39: // ArrowRight
            //    //this.ar = false;
            //    break;
        }
    }

    public move(context: DrawingContext): void {
        if (this.w  && context.isSquareInside(this,  0, -5)) this.y -= 5;
        if (this.a  && context.isSquareInside(this, -5,  0)) this.x -= 5;
        if (this.s  && context.isSquareInside(this,  0,  5)) this.y += 5;
        if (this.d  && context.isSquareInside(this,  5,  0)) this.x += 5;

        this.updateDirection();
    }

    public createShot(): Shot {
        let shot: Shot = new Shot(this);
        shot.move();
        return shot;
    }

    public draw(context: DrawingContext): void {
        context.drawPolygon(this.getRotationPoints([140, 80, 140]));
    }
};