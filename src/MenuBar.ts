import { Difficulty } from "./EnemySpawner.js";
import { ShotControl } from "./Player.js";
import { Highscore, highscoreStorage } from "./HighscoreStorage.js";

export class MenuBar {
    private static readonly music: HTMLAudioElement = new Audio("./resources/Music.wav");
    private static readonly lazer: HTMLAudioElement = new Audio("./resources/Lazer.wav");

    private menuBar: HTMLDivElement;
    private startGameButton: HTMLButtonElement;
    private difficultyFieldset: HTMLFieldSetElement;
    private shotcontrolsFieldset: HTMLFieldSetElement;
    private soundeffectCheckbox: HTMLInputElement;
    private musicCheckbox: HTMLInputElement;
    private playerNameTextBox: HTMLInputElement;
    private scoreDisplay: HTMLDivElement;
    private showHighscoreButton: HTMLButtonElement;

    private overlayBackground: HTMLDivElement;
    private overlayContent: HTMLDivElement;
    private closeOverlay: HTMLSpanElement;
    private highscoreTable: HTMLTableElement;

    private score: number = 0;
    public isGamePaused: boolean = false;
    public isGameOver: boolean = true;

    public static get HTML() {
        return "<div id='overlaybackground'>" +
                    "<div id='overlaycontent'>" +
                        "<span id='closeoverlay'>&times;</span>" +
                        "<table id='highscoretable'></table>" +
                    "</div>" +
                "</div>" +
                "<div id='menu'>" +
                    "<button id='startgame'>Start Game</button>" +
                    "<fieldset id='difficulty'><legend>Difficulty</legend>" +
                        "<input type='radio' id='difficultyeasy' name='difficulty' value='easy'><label for='difficultyeasy'>Easy</label> " +
                        "<input type='radio' id='difficultymedium' name='difficulty' value='medium' checked='checked'><label for='difficultymedium'>Medium</label> " +
                        "<input type='radio' id='difficultyhard' name='difficulty' value='hard'><label for='difficultyhard'>Hard</label>" +
                    "</fieldset>" +
                    "<fieldset id='shotcontrols'><legend>Shot controls</legend>" +
                        "<input type='radio' id='shotcontrolsmouse' name='shotcontrols' value='mouse' checked='checked'><label for='shotcontrolsmouse'>Mouse</label> " +
                        "<input type='radio' id='shotcontrolsarrowkeys' name='shotcontrols' value='arrowkeys'><label for='shotcontrolsarrowkeys'>Arrow keys</label>" +
                    "</fieldset>" +
                    "<div>" +
                        "<input type='checkbox' id='music' value='music' checked='checked'><label for='music'>Music</label>" +
                    "</div> " +
                    "<div>" +
                        "<input type='checkbox' id='soundeffects' value='soundeffects' checked='checked'><label for='soundeffects'>Soundeffects</label>" +
                    "</div>" +
                    "<div>" +
                        "<label for='playername'>Player name:</label> <input type='text' id='playername'>" +
                    "</div>" +
                    "<div id='scoredisplay'>Score: 0</div>" +
                    "<button id='showhighscore'>Show highscore</button>" +
                "</div>";
    }
    public get Difficulty() {
        let e: HTMLInputElement | null = this.difficultyFieldset.querySelector("input[name='difficulty']:checked");
        if (e == null) return Difficulty.Medium; // not needed when alternative switch statement is used
        switch (e.value) {
            // switch (this.difficultyFieldset.querySelector<HTMLInputElement>("input[name='difficulty']:checked")?.value) in TypeScript 3.7
            case "easy":
                return Difficulty.Easy;
            case "medium":
                return Difficulty.Medium;
            case "hard":
                return Difficulty.Hard;
            default:
                return Difficulty.Medium;
        }
    }
    public set Difficulty(difficulty: Difficulty) {
        switch (difficulty) {
            case Difficulty.Easy:
                MenuBar.getHTMLElementById<HTMLInputElement>("difficultyeasy").checked = true;
                break;
            case Difficulty.Medium:
                MenuBar.getHTMLElementById<HTMLInputElement>("difficultymedium").checked = true;
                break;
            case Difficulty.Hard:
                MenuBar.getHTMLElementById<HTMLInputElement>("difficultyhard").checked = true;
                break;
            default:
                MenuBar.getHTMLElementById<HTMLInputElement>("difficultymedium").checked = true;
                break;
        }
    }
    public get ShotControl() {
        let e: HTMLInputElement | null = this.shotcontrolsFieldset.querySelector("input[name='shotcontrols']:checked");
        if (e == null) return ShotControl.Mouse; // not needed when alternative switch statement is used
        switch (e.value) {
            // switch (this.shotcontrolsFieldset.querySelector<HTMLInputElement>("input[name='shotcontrols']:checked")?.value) in TypeScript 3.7
            case "mouse":
                return ShotControl.Mouse;
            case "arrowkeys":
                return ShotControl.ArrowKeys;
            default:
                return ShotControl.Mouse;
        }
    }
    public set ShotControl(shotControl: ShotControl) {
        switch (shotControl) {
            case ShotControl.Mouse:
                MenuBar.getHTMLElementById<HTMLInputElement>("shotcontrolsmouse").checked = true;
                break;
            case ShotControl.ArrowKeys:
                MenuBar.getHTMLElementById<HTMLInputElement>("shotcontrolsarrowkeys").checked = true;
                break;
            default:
                MenuBar.getHTMLElementById<HTMLInputElement>("shotcontrolsmouse").checked = true;
                break;
        }
    }
    public get PlayerName() { return this.playerNameTextBox.value; }
    public set PlayerName(name: string) { this.playerNameTextBox.value = name; }
    public get Score() { return this.score; }
    public set Score(score: number) { this.score = score; this.scoreDisplay.innerText = "Score: " + score; }

    private static getHTMLElementById<T extends HTMLElement>(id: string): T {
        let element: HTMLElement | null = document.getElementById(id);
        if (element == null) throw new TypeError("The HTMLElement with the id '" + id + "' could not be obtained!");
        return <T>element;
    }

    constructor() {
        this.menuBar = MenuBar.getHTMLElementById("menu");
        this.menuBar.style.backgroundColor = "lightgrey";
        this.menuBar.style.display = "flex";
        this.menuBar.style.alignItems = "center";
        this.menuBar.style.flexDirection = "row";
        this.menuBar.style.justifyContent = "space-around";

        this.startGameButton = MenuBar.getHTMLElementById("startgame");
        this.difficultyFieldset = MenuBar.getHTMLElementById("difficulty");
        this.shotcontrolsFieldset = MenuBar.getHTMLElementById("shotcontrols");
        this.musicCheckbox = MenuBar.getHTMLElementById("music");
        this.soundeffectCheckbox = MenuBar.getHTMLElementById("soundeffects");
        this.playerNameTextBox = MenuBar.getHTMLElementById("playername");
        this.scoreDisplay = MenuBar.getHTMLElementById("scoredisplay");
        this.showHighscoreButton = MenuBar.getHTMLElementById("showhighscore");

        this.overlayBackground = MenuBar.getHTMLElementById("overlaybackground");
        this.overlayBackground.style.display = "none";
        this.overlayBackground.style.position = "fixed";
        this.overlayBackground.style.zIndex = "1";
        this.overlayBackground.style.left = "0";
        this.overlayBackground.style.right = "0";
        this.overlayBackground.style.width = "100%";
        this.overlayBackground.style.height = "100%";
        this.overlayBackground.style.backgroundColor = "rgb(0, 0, 0)";
        this.overlayBackground.style.backgroundColor = "rgb(0, 0, 0, 0.4)"
        this.overlayBackground.addEventListener("click", this.hideOverlayHandler.bind(this));

        this.overlayContent = MenuBar.getHTMLElementById("overlaycontent");
        this.overlayContent.style.backgroundColor = "lightgrey";
        this.overlayContent.style.margin = "auto";
        this.overlayContent.style.padding = "20px";
        this.overlayContent.style.width = "80%";
        this.overlayContent.style.overflow = "auto";

        this.closeOverlay = MenuBar.getHTMLElementById("closeoverlay");
        this.closeOverlay.style.cssFloat = "right";
        this.closeOverlay.style.cursor = "pointer";
        this.closeOverlay.style.fontSize = "24px";
        this.closeOverlay.style.fontWeight = "bold";
        this.closeOverlay.style.backgroundColor = "inherit";

        this.highscoreTable = MenuBar.getHTMLElementById("highscoretable");
        this.highscoreTable.style.textAlign = "center";
        this.highscoreTable.style.margin = "auto";
        this.highscoreTable.style.width = "90%";

        this.Difficulty = highscoreStorage.LastUsedDifficulty;
        this.PlayerName = highscoreStorage.LastUsedPlayerName;

        this.musicCheckbox.addEventListener("change", this.musicCheckboxChangeHandler.bind(this));
        this.showHighscoreButton.addEventListener("click", this.showHighscoreClickHandler.bind(this));

        this.windowResizeHandler();
        window.addEventListener("resize", this.windowResizeHandler.bind(this));
    }

    private windowResizeHandler(): void {
        this.menuBar.style.height = Math.max(Math.floor(document.documentElement.clientHeight * 0.08), 54) + "px";

        let paddingTop: number = Math.round(document.documentElement.clientHeight * 0.05);

        this.overlayBackground.style.paddingTop = paddingTop + "px";
        this.overlayContent.style.maxHeight = document.documentElement.clientHeight - paddingTop * 2 - 40 + "px";
    }

    private musicCheckboxChangeHandler(): void {
        if (this.musicCheckbox.checked) {
            if (!this.isGamePaused && !this.isGameOver)
                this.startMusic();
        } else
            this.stopMusic();
    }

    private showHighscoreClickHandler(): void {
        let tableContent: string = "<thead><tr><th>Player name</th><th>Score</th><th>Date</th></tr></thead>";

        tableContent += "<tbody>";
        highscoreStorage.getOrderedHighscores(this.Difficulty).forEach((highscore: Highscore): void => {
            tableContent += `<tr><td>${highscore.playerName}</td><td>${highscore.score}</td><td>${new Date(highscore.date).toLocaleString()}</td></tr>`;
        });
        tableContent += "</tbody>";

        this.highscoreTable.innerHTML = tableContent;

        this.overlayBackground.style.display = "block";
    }

    private hideOverlayHandler(event: MouseEvent): void {
        if (event.target == this.closeOverlay || event.target == this.overlayBackground)
            this.overlayBackground.style.display = "none";
    }

    public addStartGameClickedListener(listener: (event: MouseEvent) => any): void {
        this.startGameButton.addEventListener("click", listener);
    }

    public removeStartGameClickedListener(listener: (event: MouseEvent) => any): void {
        this.startGameButton.removeEventListener("click", listener);
    }

    public addShotcontrolsChangedListener(listener: (event: Event) => any): void {
        this.shotcontrolsFieldset.addEventListener("change", listener);
    }

    public removeShotcontrolsChangedListener(listener: (event: Event) => any): void {
        this.shotcontrolsFieldset.removeEventListener("change", listener);
    }

    public startMusic(): void {
        if (!this.musicCheckbox.checked) return;
        MenuBar.music.loop = true;
        MenuBar.music.play();
    }

    public pauseMusic(): void {
        MenuBar.music.pause();
    }

    public stopMusic(): void {
        MenuBar.music.pause();
        MenuBar.music.currentTime = 0;
    }

    public playLazerSound(): void {
        if (this.soundeffectCheckbox.checked)
            (<HTMLAudioElement>MenuBar.lazer.cloneNode(false)).play();
    }
}
